package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"strconv"

	"gitlab.com/jrobsonchase/mumble-position-debug/MurmurRPC"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"
	"layeh.com/gumble/gumble"
	_ "layeh.com/gumble/opus"
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "usage: %s [flags]\n", os.Args[0])
		flag.PrintDefaults()
	}
	user := flag.String("user", "debug", "username")
	pass := flag.String("pass", "password", "password")
	channel := flag.String("chan", "Root", "channel to join")
	host := flag.String("host", "localhost", "host to connect to")
	clientPort := flag.Int("port", gumble.DefaultPort, "port for client connection")
	grpcPort := flag.Int("grpc", 50051, "port for grpc connection")
	game := flag.String("game", "Manual placement", "game for position context")
	ctx := flag.String("ctx", "Mumble", "context for position")
	id := flag.String("id", "debug", "id for position")
	flag.Parse()

	config := gumble.NewConfig()
	config.Username = *user
	config.Password = *pass
	_ = config.AudioListeners.Attach(&debugListener{})
	conn, err := gumble.DialWithDialer(new(net.Dialer), net.JoinHostPort(*host, fmt.Sprint(*clientPort)), config, &tls.Config{
		// The default cert is self-signed and will fail Go's tls verification
		InsecureSkipVerify: true,
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", os.Args[0], err)
		os.Exit(1)
	}
	if *channel != "Root" {
		ch := conn.Channels.Find(*channel)
		if ch != nil {
			conn.Self.Move(ch)
		} else {
			log.Fatalf("couldn't find channel %s", *channel)
		}
	}

	grpcConn, err := grpc.Dial(net.JoinHostPort(*host, fmt.Sprint(*grpcPort)), grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer grpcConn.Close()

	server := &MurmurRPC.Server{
		Id: proto.Uint32(1),
	}

	streamClient := MurmurRPC.NewV1Client(grpcConn)
	stream, err := streamClient.ServerEvents(context.Background(), server)

	// Get the initial state of the connected users that are in the same channel
	// as the debug user
	ch := uint32(0)
	if users, err := streamClient.UserQuery(context.Background(), &MurmurRPC.User_Query{
		Server: server,
	}); err != nil {
		log.Fatalf("error getting user list: %v", err)
	} else {
		for _, u := range users.Users {
			if u.GetName() == *user {
				ch = u.GetChannel().GetId()
				break
			}
		}
		for _, u := range users.Users {
			if u.GetName() != *user && u.GetChannel().GetId() == ch {
				logUserInfo(u)
			}
		}
	}

	// Per the docs:
	// The official Mumble client sets the context to:
	// PluginShortName + "\x00" + AdditionalContextInformation
	// Do this after the stream of events has started so that we get our own
	// id/context update.
	conn.Self.SetPlugin([]byte(fmt.Sprintf("%s\x00%s", *game, *ctx)), *id)

	for {
		event, err := stream.Recv()
		if err != nil {
			log.Fatalf("event stream ended with error: %v", err)
		}

		if event.GetType() != MurmurRPC.Server_Event_UserStateChanged {
			continue
		}

		if event.GetUser().GetChannel().GetId() != ch {
			continue
		}

		logUserInfo(event.GetUser())
	}
}

func logUserInfo(u *MurmurRPC.User) {
	log.Printf("%s: id: %s, context: %s",
		u.GetName(),
		// Quote these so we know they'll print nicely.
		// Context will definitely contain a null byte, and identity can
		// have arbitrary utf8
		strconv.Quote(u.GetPluginIdentity()),
		strconv.Quote(string(u.GetPluginContext())),
	)
}

type debugListener struct{}

func (dl *debugListener) OnAudioStream(e *gumble.AudioStreamEvent) {
	type userPos struct {
		X float32
		Y float32
		Z float32
	}

	// The meat is all in the channel of audio packets, so spin up a goroutine
	// to log it.
	go func() {
		// Make this all NaN to start with so it *never* matches the first time
		pos := userPos{float32(math.NaN()), float32(math.NaN()), float32(math.NaN())}

		for p := range e.C {
			newPos := userPos{
				p.X, p.Y, p.Z,
			}

			// Only log changes to avoid spam for every packet
			if pos != newPos {
				pos = newPos
				if !p.HasPosition {
					log.Printf("%s: no position data", e.User.Name)
				} else {
					log.Printf("%s: %g %g %g", e.User.Name, p.X, p.Y, p.Z)
				}
			}
		}
	}()
}
