# Mumble Positional Audio Debug Tool

Small tool to connect to a mumur server and dump positional audio data from
other users. Requires the server to be configured with GRPC support.
Currently only supports insecure GRPC connections, so best run on the same
host.

```
usage: ./mumble-position-debug [flags]
  -chan string
    	channel to join (default "Root")
  -ctx string
    	context for position (default "Mumble")
  -game string
    	game for position context (default "Manual placement")
  -grpc int
    	port for grpc connection (default 50051)
  -host string
    	host to connect to (default "localhost")
  -id string
    	id for position (default "debug")
  -pass string
    	password (default "password")
  -port int
    	port for client connection (default 64738)
  -user string
    	username (default "debug")
```

After connecting, it will connect with both a normal client and a grpc
client. The normal client will be moved to the specified channel and its
positional id and context will be set. It will then list the other users in
the channel along with their positional id and context. Updates to these
users will be streamed over GRPC and logged. The client connection will
stream audio events and log any position changes for other users.

Example output:

```
./mumble-position-debug -chan Valheim -pass password
2021/02/27 15:04:21 joshtest: id: "Agent47", context: "Manual placement\x00Mumble"
2021/02/27 15:04:21 debug: id: "debug", context: "Manual placement\x00Mumble"
2021/02/27 15:04:27 joshtest: -48.85 0 -133.85
2021/02/27 15:04:40 joshtest: id: "Agent47", context: "Manual placement\x00Something Else"
2021/02/27 15:04:43 joshtest: no position data
2021/02/27 15:04:56 joshtest: id: "Agent47", context: "Manual placement\x00Mumble"
2021/02/27 15:04:58 joshtest: -48.85 0 -133.85
2021/02/27 15:05:01 joshtest: 70.77 0 -21.54
```

## Building

`go build` should be sufficient. `go install` if you want it automatically
placed in your `$GOPATH/bin`.

## Troubleshooting

### no position data

This is due to mismatched contexts. The context for the debug tool must match
the context for any user sending position information. If they don't match,
the server won't send positions.

### GRPC connection refused

1. Make sure the `grpc` setting in `murmur.ini` is enabled
2. Check the murmur logs. If you see "grpc isn't enabled for this build,
   ignoring," then you need to install a development snapshot of the server. You
   can get it [here][devmurmur].
3. Make sure that, if it's `127.0.0.1:<port>`, you're running the tool on the
   same host as the server. You *can* set it to `0.0.0.0:<port>`, but it's not recommended for security reasons.

[devmurmur]: https://www.mumble.info/downloads/#development-snapshots